# Download Videos from Youtube Extension
Yet another browser extension to download videos from youtube (still a work in progress).
Currently this extension does not work with videos that use a signature cipher.

## Sources

*  [one-liner](https://github.com/github/dmca/commit/bccf7d0dbfec423c4a967f668be47b6339d15893#commitcomment-43529358) using curl and perl to download a youtube video by Github user [InfoTeddy](https://github.com/InfoTeddy)

*  A simple [python youtube download script](https://github.com/sdushantha/bin/blob/master/utils/ytdl.py) using minimal dependancies by Github user [sdushantha](https://github.com/sdushantha)
