function getFormats(streamingData) {
    let formats = [];
    streamingData.formats.forEach(e => formats.push(e));
    if (typeof streamingData.adaptiveFormats !== null) {
        streamingData.adaptiveFormats.forEach(e => formats.push(e));
    }
    return formats;
}

let messageHandler = (...args) => {};
let buttonPressed = () => {};
function onMessageCallback(req, send, response) {
    if (req === 'new-page') {
        console.log('new-page');
        main();
    } else {
        messageHandler(req, send, response);
    }
}

browser.runtime.onMessage.addListener(onMessageCallback);

window.addEventListener("yt-navigate-finish", () => {
    console.log("nav-finished"); 
    let send = browser.runtime.sendMessage({reload: 'reload'});
    send.then(acc => console.log(acc), err => console.log(err));
});


let docString;
let reString = null;
let formats;
let urls = [];
let signatures = [];
let videoTitle = "";
let jsUrlObject = null;
let mutObserver;


function getStuff() {
    docString = document.body.innerHTML.toString();
    reString = null;
    let regExps = [
        new RegExp("ytplayer\.config[ ]*=[ ]*\{.*\};ytplayer"),
        new RegExp("ytInitialPlayerResponse[ ]*=[ ]*{.*};var[ ]*meta")
    ];
    do {
        // In chrome, if the string match method does not find
        // anything, it returns null instead on an empty array.
        let docStringMatch = docString.match(regExps.pop());
        if (docStringMatch) {
            reString = docStringMatch[0];
        }
    } while(!reString);
    let endCutPos;
    for (endCutPos = reString.length - 1; reString[endCutPos] !== ';'; endCutPos--);
    let jsonString = reString.substring(reString.indexOf("{"), endCutPos);
    let jsonData = JSON.parse(jsonString);
    if (typeof jsonData.args !== 'undefined') {
        let jsonData2 = JSON.parse(jsonData.args.player_response);
        formats = getFormats(jsonData2.streamingData);
    } else if (typeof jsonData.streamingData !== 'undefined') {
        formats = getFormats(jsonData.streamingData);
    }
    urls = [];
    signatures = [];
    for (let e of formats) {
        if (typeof e.url !== 'undefined') {
            urls.push(e.url);
        }
        if (typeof e.signatureCipher !== 'undefined') {
            signatures.push(e.signatureCipher);
        }
    }
    let regExp = new RegExp(`"jsUrl"[ ]*:[ ]*"[^"]*"`);
    let jsUrlString = docString.match(regExp)[0];
    if (jsUrlString) {
        jsUrlObject = JSON.parse("{" + docString.match(regExp)[0] + "}");
    }
    videoTitle = "";
    regExp = new RegExp(`<title>[^<>]*</title>`);
    try {
    videoTitle = docString.match(regExp)[0].replace(
            "<title>", "").replace("</title>", "");
    } catch(e) {console.log(e);}
}


function main() {

    console.log('new-page');
    /*let observerFunc = (mutations, obs) => {
        console.log('mutation');
        // for (let m of mutations) {
        //     console.log(m);
        // }
    };
    mutObserver = new MutationObserver(observerFunc);
    mutObserver.observe(document, {attributes: true, childList: true, subtree: true});*/
    // getStuff();
    document.addEventListener("DomContentLoaded", getStuff);
    document.addEventListener("spfdone", getStuff);
    document.body.addEventListener("transitionend", getStuff);
    document.documentElement.addEventListener("transitionend", getStuff);

    buttonPressed = () => {
        console.log(videoTitle);
        console.log(urls, signatures);
        console.log(formats);
        browser.runtime.sendMessage(
            message={title: videoTitle, 
                    formats: formats, jsUrl: jsUrlObject.jsUrl});
    }


    messageHandler = (req, send, response) => {
        console.log(req);
        if (req === 'button-pressed') {
            buttonPressed();
        } else if (req === 'load-html') {
            browser.runtime.sendMessage(message={reload: 'reload'});
            try {
                getStuff();
            } catch (e) {console.log(e);}
            let htmlString = ``;
            for (let format of formats) {
                let itag = format.itag;
                let tooltip = "";
                for (let field of Object.keys(format)) {
                    if (field !== 'itag' && field !== 'url' && 
                        field !== 'initRange' && field !== 'indexRange') {
                        if (field === 'mimeType') {
                            tooltip += field + ": " + format[field].split(';')[0] + "\n";
                        } else {
                            tooltip += field + ": " + format[field] + "\n";
                        }
                    }
                }
                let checkboxElement = `<div title="${tooltip}">
                                       <input type="checkbox" id="${itag}" name="${itag}">
                                       <label for="${itag}">${itag}</label>
                                       </div>`;
                htmlString += checkboxElement;
            }
            browser.runtime.sendMessage(message={html: htmlString});
        }
    }
}

main();