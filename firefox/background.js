const NAMESPACE_DOT = "browser.";


function onUpdated() {
    // let tabsFunc = tabs => browser.tabs.sendMessage(tabs[0].id, 'new-page', null);
    let tabsFunc = tabs => browser.tabs.executeScript(tabs[0].id, 
         {file: '/content.js'});
    // tabsFunc();
    if (NAMESPACE_DOT.includes('browser')) {
        let querying = browser.tabs.query({active: true, currentWindow: true});
        querying.then(tabsFunc);
    } else if (NAMESPACE_DOT.includes('chrome')){
        browser.tabs.query({active: true, currentWindow: true}, tabsFunc);
    }   
}

browser.runtime.onMessage.addListener((message, sender, sendResponse) => {
    if (typeof message.reload !== 'undefined') {
        sendResponse('message recieved');
        let tabsFunc = tabs => browser.tabs.update(tabs[0].id, 
            {url: tabs[0].url});
        let querying = browser.tabs.query({active: true, currentWindow: true});
        querying.then(tabsFunc);
        // return Promise.resolve.then(onUpdated);
    }
})



// browser.webNavigation.onCompleted.addListener(onUpdated);
// browser.webNavigation.onUpdated.addListener(onUpdated);
// browser.webNavigation.onMessage.addListener(onUpdated);
// browser.webNavigation.onReferenceFragmentUpdated(onUpdated);
// browser.webNavigation.onHistoryStateUpdated.addListener(onUpdated);
// browser.webNavigation.onTabReplaced.addListener(onUpdated);
// browser.webNavigation.onDOMContentLoaded(onUpdated);
// browser.tabs.onUpdated.addListener(onUpdated);
