const NAMESPACE_DOT = "browser.";
let btn = document.getElementById('pressy');


function removeInvalidFilenameCharacters(string="") {
    for (let c of ['<', '>', ':', '?', '\"', '\\',
                   '/', '|', '*', "\"", "'"]) {
        while (string.includes(c)) {
            string = string.replace(c, '');
        }
    }
    return string;
}


browser.runtime.onMessage.addListener(message => {
    if (typeof message.formats !== 'undefined') {
        for (let i = 0; i < message.formats.length; i++) {
            let format = message.formats[i];
            let checkbox = document.getElementById(`${format.itag}`);
            if (checkbox.checked) {
                let downloadLink;
                if (typeof format.url !== 'undefined') {
                    downloadLink = format.url;
                    let title = removeInvalidFilenameCharacters(message.title);
                    try {
                        browser.downloads.download({url: downloadLink, filename: 
                                                `${title}.mkv`});
                    } catch (e) {
                        browser.downloads.download({url: downloadLink, filename: 
                            `${Date.now()}.mkv`});
                    }
                } else if (typeof format.signatureCipher !== 'undefined') {
                    // TODO
                    if (message.jsUrl) {
                        alert(`Haven't dealt with ciphered content yet :(\n${message.jsUrl}`);
                        let jsUrl = message.jsUrl;
                        
                    }
                }
            }
        }
    }
    if (typeof message.html !== 'undefined') {
        let checkboxes = document.getElementById('checkboxes');
        checkboxes.innerHTML = message.html;
    }
});


/* Sending a message from this script to the content script:
https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/
WebExtensions/Modify_a_web_page#Messaging 
first code example.*/
function sendMessage(msg, respFunc) {
    let tabsFunc = tabs => browser.tabs.sendMessage(tabs[0].id, msg, respFunc);
    if (NAMESPACE_DOT.includes('browser')) {
        let querying = browser.tabs.query({active: true, currentWindow: true});
        querying.then(tabsFunc);
    } else if (NAMESPACE_DOT.includes('chrome')){
        browser.tabs.query({active: true, currentWindow: true}, tabsFunc);
    }
}

Promise.resolve(sendMessage('load-html', null));

btn.addEventListener('click', () => {
    sendMessage('button-pressed', null);
})