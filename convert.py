"""
Convert extension files to either be compatible
with chrome or with firefox.
"""
import glob
import re
import sys


def replace(new_word, pattern):
    files = glob.glob("./*/*.js", recursive=True)
    for f in files:
        lines = []
        reg = re.compile(pattern)
        with open(f, "r") as js_file:
            for line in js_file:
                newline = reg.sub(new_word, line)
                print(newline, end='')
                lines.append(newline)
        with open(f, "w") as js_file:
            for line in lines:
                js_file.write(line)
            

if sys.argv[1] == 'chrome':
    replace('chrome.', 'browser\\.')
if sys.argv[1] == 'firefox':
    replace('browser.', 'chrome\\.')